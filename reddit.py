import os
from enum import Enum
from typing import List

import requests, re

from story import Story

REDDIT_URL = "https://reddit.com"
SUBREDDIT_URL = os.getenv("SUBREDDIT_URL", "/r/nosleep")

HEADERS = {"User-Agent": "epub-fetcher"}


class Tabs(Enum):
    HOT = "hot"
    NEW = "new"
    RISING = "rising"
    CONTROVERSIAL = "controversial"
    TOP = "top"
    GILDED = "gilded"

    @staticmethod
    def from_str(string: str) -> "Tabs":
        # Is there a better way to convert a string to an enum?
        map = {
            "hot": Tabs.HOT,
            "new": Tabs.NEW,
            "rising": Tabs.RISING,
            "controversial": Tabs.CONTROVERSIAL,
            "top": Tabs.TOP,
            "gilded": Tabs.GILDED,
        }

        return map.get(string.lower(), Tabs.HOT)


class Top(Enum):
    HOUR = "hour"
    DAY = "day"
    WEEK = "week"
    MONTH = "month"
    YEAR = "year"
    ALL = "all"

    @staticmethod
    def from_str(string: str) -> "Top":
        # Is there a better way to convert a string to an enum?
        map = {
            "hour": Top.HOUR,
            "day": Top.DAY,
            "week": Top.WEEK,
            "month": Top.MONTH,
            "year": Top.YEAR,
            "all": Top.ALL,
        }

        return map.get(string.lower(), Top.MONTH)


def fetch_post(url: str) -> Story:
    """Fetch an URL and extract the post as a Story.

    Raises a ValueError if the URL is not a Reddit URL.
    Raises a RuntimeError if the result status code was not 200.
    Raises a RuntimeError if the retrieved post does not have self text."""
    pattern = re.compile("^http?s://(www\.)?reddit\.com", re.IGNORECASE)

    if not pattern.match(url):
        raise ValueError(f"Not a valid Reddit URL: {url}")

    if not url.endswith(".json"):
        url += ".json"

    response = requests.get(url, headers=HEADERS)

    if response.status_code != 200:
        raise RuntimeError(
            f"Could not retrieve post. Status code was not 200: {response.status_code}"
        )

    json = response.json()

    # The response for a single post will be a list with two children.
    # 1. The actual self-post contained in another `children` property
    # 2. Its comments

    # Get the post
    post = json[0]["data"]["children"][0]["data"]

    if not post["is_self"] or not post["selftext"]:
        raise RuntimeError("Post is not a self-post (does not have any text).")

    return Story.from_json(post)


def fetch_posts(
    from_tab: Tabs = Tabs.HOT,
    take: int = 10,
    top: Top = Top.WEEK,
    include_series: bool = True,
) -> List[Story]:
    """Fetch a number of posts as Stories."""
    if take < 1:
        raise ValueError("Must fetch at least one story")

    url = build_url(from_tab, top)

    response = requests.get(url, headers=HEADERS)
    json = response.json()

    posts = json["data"]["children"]

    stories = []

    for post in posts:
        # Skip stickied posts
        if post["data"]["stickied"]:
            continue

        # Skip series
        if not include_series and post["data"]["link_flair_text"] == "Series":
            continue

        # Add it to the stories if it's a self-post
        if post["data"]["is_self"]:
            stories.append(Story.from_json(post["data"]))

        # Break if we have gathered enough posts
        if len(stories) >= take:
            break

    return stories


def build_url(from_tab: Tabs = Tabs.HOT, top: Top = Top.WEEK,) -> str:
    """Build a API URL for the given parameters."""
    url = REDDIT_URL + SUBREDDIT_URL

    url += f"/{from_tab.value}.json"

    if from_tab is Tabs.TOP:
        url += "?t=" + top.value

    return url
