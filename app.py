from typing import List

from flask import Flask, render_template, request, abort, send_file

import epub
import reddit
import tempfile

from story import Story

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/download", methods=["POST"])
def download_bulk():
    tab = request.form.get("tab")
    take = request.form.get("take")
    top = request.form.get("top", "month")
    include_series = bool(request.form.get("include-series"))

    if not tab:
        abort(400)

    try:
        take = int(take)
    except TypeError:
        take = 10

    tab = reddit.Tabs.from_str(tab)
    top = reddit.Top.from_str(top)

    stories = reddit.fetch_posts(tab, take, top, include_series)

    return download_stories(stories)


@app.route("/downloadone", methods=["POST"])
def download_one():
    url = request.form.get("url")

    if not url:
        abort(400)

    try:
        story = reddit.fetch_post(url)
    except (ValueError, RuntimeError):
        abort(400)

    return download_stories([story])


def download_stories(stories: List[Story]):
    file = tempfile.NamedTemporaryFile(delete=False)
    book = epub.from_stories(stories, filename=file.name)

    return send_file(
        file.name,
        mimetype="application/epub+zip",
        as_attachment=True,
        attachment_filename="nosleep.epub",
    )
