import os
from typing import List, IO
import uuid

from ebooklib.epub import EpubBook, EpubHtml, EpubNcx, EpubNav, Link, write_epub
from markdown import markdown

from story import Story

LANG = os.getenv("LANG", "en")


def from_stories(
    stories: List[Story], book_title: str = None, filename: str = None
) -> EpubBook:
    """Make an EPUB file from a list of Story objects.

    One story equals one chapter."""
    book = EpubBook()

    if len(stories) == 1:
        book.set_identifier(stories[0].id)
        if not book_title:
            book_title = book.set_title(stories[0].title)
    else:
        book.set_identifier(str(uuid.uuid4()))
        if not book_title:
            book_title = book.set_title(
                "r/nosleep: A Collection of {} Stories".format(len(stories))
            )

    book.set_title(book_title)
    book.set_language(LANG)

    book.spine = []
    book.toc = []
    for story in stories:
        book.add_author(story.author)
        chapter = story_to_html(story)

        book.add_item(chapter)
        book.spine.append(chapter)
        book.toc.append(Link(chapter_file_name(story), story.title, story.id))

    book.add_item(EpubNcx())
    book.add_item(EpubNav())

    # Write to this file if filename is set.
    # Passing a file object would be better but ebooklib's write_epub only supports
    # passing file names.
    if filename:
        write_epub(filename, book)

    return book


def story_to_html(story: Story) -> EpubHtml:
    """Convert a Story object's text and title to an
    HTML representation we can use as EPUB chapters."""
    chapter = EpubHtml(title=story.title, file_name=chapter_file_name(story), lang=LANG)

    text = markdown(story.text)

    html = f"""
    <html>
        <body>
            <h1>{story.title}</h1>
            <p><i>Written by <a href="https://reddit.com/{story.author}">{story.author}</a></i></p>
            {text}
        </body>
    </html>
    """

    chapter.set_content(html)

    return chapter


def chapter_file_name(story: Story) -> str:
    return f"{story.id}.xhtml"
