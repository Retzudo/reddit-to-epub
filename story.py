from dataclasses import dataclass
from datetime import datetime


@dataclass
class Story:
    id: str
    date: datetime
    author: str
    title: str
    text: str

    @classmethod
    def from_json(cls, json: dict) -> "Story":
        """Create a Story from Reddit JSON."""
        return cls(
            id=json.get("id"),
            date=datetime.fromtimestamp(json.get("created")),
            author="u/" + json.get("author"),
            title=json.get("title"),
            text=json.get("selftext"),
        )
